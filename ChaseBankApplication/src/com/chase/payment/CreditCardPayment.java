package com.chase.payment;

public class CreditCardPayment {
	
	private int id;
	private String creditCardNumber;
	private String expirationDate;
	private String cvvCode;
	private String holderName;
	
	
	public CreditCardPayment() {
		super();
	}
	public CreditCardPayment(int id, String creditCardNumber, String expirationDate, String cvvCode,
			String holderName) {
		super();
		this.id = id;
		this.creditCardNumber = creditCardNumber;
		this.expirationDate = expirationDate;
		this.cvvCode = cvvCode;
		this.holderName = holderName;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCreditCardNumber() {
		return creditCardNumber;
	}
	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}
	public String getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}
	public String getCvvCode() {
		return cvvCode;
	}
	public void setCvvCode(String cvvCode) {
		this.cvvCode = cvvCode;
	}
	public String getHolderName() {
		return holderName;
	}
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}
	
	
}
