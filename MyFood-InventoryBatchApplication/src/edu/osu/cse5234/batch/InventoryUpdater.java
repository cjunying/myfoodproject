package edu.osu.cse5234.batch;

import java.sql.Array;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class InventoryUpdater {
	public static void main(String[] args) {

		System.out.println("Starting Inventory Update ...");
		try {
			Connection conn = createConnection();
			Collection<Integer> newOrderIds = getNewOrders(conn);
			Map<Integer, Integer> orderedItems = getOrderedLineItems(newOrderIds, conn);
			udpateInventory(orderedItems, conn);
			udpateOrderStatus(newOrderIds, conn);
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static Connection createConnection() throws SQLException, ClassNotFoundException {
		Class.forName("org.h2.Driver");
		Connection conn = DriverManager.getConnection("jdbc:h2:C:/Documents/workspace/cse5234/h2db/MyFoodDB", "sa", "");
		return conn;
	}

	private static Collection<Integer> getNewOrders(Connection conn) throws SQLException {
		Collection<Integer> orderIds = new ArrayList<Integer>();
		ResultSet rset = conn.createStatement().executeQuery(
                     "select ID from CUSTOMER_ORDER where STATUS = 'New'");
		while (rset.next()) {
			orderIds.add(new Integer(rset.getInt("ID")));
		}
		return orderIds;
	}

	private static Map<Integer, Integer> getOrderedLineItems(Collection<Integer> newOrderIds,
                Connection conn)  throws SQLException {
		// This method returns a map of two integers. The first Integer is item ID, and 
                 // the second is cumulative requested quantity across all new orders
		Map<Integer, Integer> orderLineItems = new HashMap<>();
		
		Iterator<Integer> orderId = newOrderIds.iterator();
		while (orderId.hasNext()) {
			int temp = orderId.next();
			String queryState = "select ITEM_ID,QUANTITY from CUSTOMER_ORDER_LINE_ITEM where CUSTOMER_ORDER_ID_FK = " 
					 + temp ;
			ResultSet rset = conn.createStatement().executeQuery(queryState);
			while (rset.next()) {
				int itemId = rset.getInt("ITEM_ID");
				int quantity = rset.getInt("QUANTITY");
				if (orderLineItems.containsKey(itemId)) {
					int quantity_temp = orderLineItems.get(itemId);
					quantity += quantity_temp;
				}
				orderLineItems.put(itemId, quantity);
			}
		}
		return orderLineItems;
	}

	private static void udpateInventory(Map<Integer, Integer> orderedItems, 
                Connection conn) throws SQLException {
		// TODO Auto-generated method stub
		String query = "select ITEM_NUMBER, AVAILABLE_QUANTITY from ITEM";
		ResultSet rset = conn.createStatement().executeQuery(query);
		while (rset.next()) {
			int itemId = rset.getInt("ITEM_NUMBER");
			int quantity = rset.getInt("AVAILABLE_QUANTITY");
			int orderNum = orderedItems.get(itemId);
			quantity = quantity - orderNum;
			query = "update ITEM set AVAILABLE_QUANTITY = " + quantity + "where ITEM_NUMBER = "  + itemId ;
			conn.createStatement().executeUpdate(query);
		}

	}

	private static void udpateOrderStatus(Collection<Integer> newOrderIds, 
                Connection conn) throws SQLException {
		// TODO Auto-generated method stub
		Iterator<Integer> orderId = newOrderIds.iterator();
		while (orderId.hasNext()) {
			int id = orderId.next();
			String query = "update CUSTOMER_ORDER set STATUS = 'Processed' where ID = "  + id ;
			conn.createStatement().executeUpdate(query);
		}

	}

}


